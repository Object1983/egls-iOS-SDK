#import "AppDelegate.h"
#import "UIViewController+testC.h"
#import "EGLSSDKUtil.h"
#import <AdSupport/AdSupport.h>
#define MYBUNDLE_QUOTENAME @"EGLSSDKRES.bundle"
#define MYBUNDLE_PATH [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:MYBUNDLE_QUOTENAME]
#define MYBUNDLE [NSBundle bundleWithPath: MYBUNDLE_PATH]
#define LS(str, nil) NSLocalizedStringFromTableInBundle(str, nil, MYBUNDLE, nil)

#import "SDK_AppleCharge.h"
//#import "SVProgressHUD/SVProgressHUD.h"

#import "BDAutoTrack.h"
#import "BDAutoTrackConfig.h"

@implementation AppDelegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor grayColor];
    [self.window makeKeyAndVisible];
    
    testC * vc = [[testC alloc] init];
    self.window.rootViewController = vc;
    //    [Bugly startWithAppId:@"ee092f177b"];
    //    [Bugly startWithAppId:@"28dbffb447"];
    
    NSLog(@"testC = %@ view = %@",vc,vc.view);
    
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    if (screenWidth < screenHeight) {
        CGFloat tmp = screenWidth;
        screenWidth = screenHeight;
        screenHeight = tmp;
    }
    CGFloat size = screenHeight * 0.0006;
    
    CGFloat x_btn = 100 * size;
    CGFloat y = 0;
    //    CGFloat h_btn = 40 * size;
    CGFloat h_btn = 0.1 * screenHeight;
    //    CGFloat w_btn = 400 * size;
    CGFloat w_btn = h_btn * 4;
    
    //    CGFloat y_alpha = 20 * size;
    CGFloat y_alpha = (screenHeight - h_btn * 6) / 7;
    CGFloat x_alpha = 100 * size;
    //===========================================================
    y = y_alpha;
    
    
    
    NSString * bundlePath = [[ NSBundle mainBundle] pathForResource:@"EGLSSDKRES" ofType :@"bundle"];
    
    NSBundle * resourceBundle = [NSBundle bundleWithPath:bundlePath];
    
    UIImage * imageBack = [UIImage imageNamed:@"EGLSSDKRES.bundle/kr_06.png"];
    imageBack = [imageBack stretchableImageWithLeftCapWidth:4 topCapHeight:4];
    
    UIButton * autoLoginButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    //    autoLoginButton.frame = CGRectMake(20, 20, 100, 20);
    autoLoginButton.frame = CGRectMake(x_btn, y, w_btn, h_btn);
    [autoLoginButton setTitle:LS(@"AutoLoin", nil) forState:UIControlStateNormal];
    [autoLoginButton setTintColor:[UIColor whiteColor]];  //LS(@"AutoLoin", nil)
    [autoLoginButton setBackgroundImage:imageBack forState:UIControlStateNormal];
    [autoLoginButton addTarget:self action:@selector(autoLoginClick) forControlEvents:UIControlEventTouchUpInside];
    [vc.view addSubview:autoLoginButton];
    
    
   
    NSString * path =  [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:MYBUNDLE_QUOTENAME];
      NSLog(@"strText path =  %@",path);
     NSString* strText =  [[NSBundle bundleWithPath: path] localizedStringForKey:@"TemporarilyDoNotBind" value:@"" table: nil];
    
    
  //   [[NSBundle bundleWithPath: path] setLanguage:@"zh-Hans"];
    
     NSLog(@"strText =  %@",strText);
    //===========================================================
    y += (h_btn + y_alpha);
    UIButton * showLoginButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    //    showLoginButton.frame = CGRectMake(20, 20, 100, 20);
    showLoginButton.frame = CGRectMake(x_btn, y, w_btn, h_btn);
    [showLoginButton setTitle:LS(@"LOGIN", nil) forState:UIControlStateNormal];
    [showLoginButton setTintColor:[UIColor whiteColor]];  //LS(@"LOGIN", nil)
    [showLoginButton setBackgroundImage:imageBack forState:UIControlStateNormal];
    [showLoginButton addTarget:self action:@selector(showLoginClick) forControlEvents:UIControlEventTouchUpInside];
    [vc.view addSubview:showLoginButton];
    
    //===========================================================
    y += (h_btn + y_alpha);
    UIButton * chargeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    //    chargeButton.frame = CGRectMake(20, 50, 100, 20);
    chargeButton.frame = CGRectMake(x_btn, y, w_btn, h_btn);
    [chargeButton setTitle:LS(@"bind", nil) forState:UIControlStateNormal];
    [chargeButton setTintColor:[UIColor whiteColor]];
    [chargeButton setBackgroundImage:imageBack forState:UIControlStateNormal];
    [chargeButton addTarget:self action:@selector(chargeClick) forControlEvents:UIControlEventTouchUpInside];
    [vc.view addSubview:chargeButton];
    
    //===========================================================
    y += (h_btn + y_alpha);
    UIButton * applePurchaseBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    //    chargeButton.frame = CGRectMake(20, 50, 100, 20);
    applePurchaseBtn.frame = CGRectMake(x_btn, y, w_btn, h_btn);
    [applePurchaseBtn setTitle:LS(@"tw.myCard", nil) forState:UIControlStateNormal];
    [applePurchaseBtn setTintColor:[UIColor whiteColor]];
    [applePurchaseBtn setBackgroundImage:imageBack forState:UIControlStateNormal];
    [applePurchaseBtn addTarget:self action:@selector(applePurchaseClick) forControlEvents:UIControlEventTouchUpInside];
    [vc.view addSubview:applePurchaseBtn];
    
    //    //===========================================================
    //    y += (h_btn + y_alpha);
    //    UIButton * logoutBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    //    logoutBtn.frame = CGRectMake(x_btn, y, w_btn, h_btn);
    //    [logoutBtn setTitle:LS(@"LOGOUT", nil) forState:UIControlStateNormal];
    //    [logoutBtn setTintColor:[UIColor whiteColor]];
    //    [logoutBtn setBackgroundImage:imageBack forState:UIControlStateNormal];
    //    [logoutBtn addTarget:self action:@selector(logoutClick) forControlEvents:UIControlEventTouchUpInside];
    //    [vc.view addSubview:logoutBtn];
    
    //    //===========================================================
    y += (h_btn + y_alpha);
    UIButton * fbShareImgBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    fbShareImgBtn.frame = CGRectMake(x_btn, y, w_btn, h_btn);
    [fbShareImgBtn setTitle:LS(@"FacebookSharePictureOnly", nil) forState:UIControlStateNormal];
    [fbShareImgBtn setTintColor:[UIColor whiteColor]];
    [fbShareImgBtn setBackgroundImage:imageBack forState:UIControlStateNormal];
    [fbShareImgBtn addTarget:self action:@selector(fbShareImgClick) forControlEvents:UIControlEventTouchUpInside];
    [vc.view addSubview:fbShareImgBtn];
    
    
    //    //===========================================================
    x_btn += (w_btn + x_alpha);   // 换列
    y = y_alpha;
    UIButton * fbShareUrlAndSoOnBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    fbShareUrlAndSoOnBtn.frame = CGRectMake(x_btn, y, w_btn, h_btn);
    [fbShareUrlAndSoOnBtn setTitle:LS(@"FacebookShareUrlAndSoON", nil) forState:UIControlStateNormal];
    [fbShareUrlAndSoOnBtn setTintColor:[UIColor whiteColor]];
    [fbShareUrlAndSoOnBtn setBackgroundImage:imageBack forState:UIControlStateNormal];
    [fbShareUrlAndSoOnBtn addTarget:self action:@selector(fbShareUrlAndSoOnClick) forControlEvents:UIControlEventTouchUpInside];
    [vc.view addSubview:fbShareUrlAndSoOnBtn];
    
    //    //===========================================================
    y += (h_btn + y_alpha);
    UIButton * lineShareImgBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    lineShareImgBtn.frame = CGRectMake(x_btn, y, w_btn, h_btn);
    [lineShareImgBtn setTitle:LS(@"LineShareImg", nil) forState:UIControlStateNormal];
    [lineShareImgBtn setTintColor:[UIColor whiteColor]];
    [lineShareImgBtn setBackgroundImage:imageBack forState:UIControlStateNormal];
    [lineShareImgBtn addTarget:self action:@selector(lineShareImgClick) forControlEvents:UIControlEventTouchUpInside];
    [vc.view addSubview:lineShareImgBtn];
    
    
    //    //===========================================================
    y += (h_btn + y_alpha);
    UIButton * lineShareTextBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    lineShareTextBtn.frame = CGRectMake(x_btn, y, w_btn, h_btn);
    [lineShareTextBtn setTitle:LS(@"LineShareText2", nil) forState:UIControlStateNormal];
    [lineShareTextBtn setTintColor:[UIColor whiteColor]];
    [lineShareTextBtn setBackgroundImage:imageBack forState:UIControlStateNormal];
    [lineShareTextBtn addTarget:self action:@selector(lineShareTextClick2) forControlEvents:UIControlEventTouchUpInside];
    [vc.view addSubview:lineShareTextBtn];
    
    y += (h_btn + y_alpha);
    UIButton * fbInvite = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    fbInvite.frame = CGRectMake(x_btn, y, w_btn, h_btn);
    [fbInvite setTitle:@"zh2.cn.6rmbtest3" forState:UIControlStateNormal];
    [fbInvite setTintColor:[UIColor whiteColor]];
    [fbInvite setBackgroundImage:imageBack forState:UIControlStateNormal];
    [fbInvite addTarget:self action:@selector(fbInviteFriends) forControlEvents:UIControlEventTouchUpInside];
    [vc.view addSubview:fbInvite];
    
    
    
    _accountField = [self getUITextField:CGRectMake(20, screenHeight*0.8+20, 150, 30) placeholder:@"account"];
    [vc.view addSubview: _accountField];
    
    _verifyField = [self getUITextField:CGRectMake(200, screenHeight*0.8+20, 150, 30) placeholder:@"verifyField"];
    [vc.view addSubview: _verifyField];
    
    
    _passWordField = [self getUITextField:CGRectMake(20, screenHeight*0.9+20, 150, 30) placeholder:@"passWordField"];
    [vc.view addSubview: _passWordField];
    
    
    
    [[EGLSSDK sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
//    8904 rwby
//    4137 ahcn com.g2us.armedheroescn
//    8803 dstw com.linkedfun.macau.tw
//    8222 zhtw com.elive.aw2tw.mc     aw2.tw.mc50twd 13552092676 123456
//    9551 com.yhc.ln100sg.ios         com.ln100.5usd
//    9155 com.egls.ssrkr.ios          超杀
//    9450 com.elive.tltc.ios          三生三世
//    8750 com.elive.aw2tw.ios1        战魂2台湾
//    8219 com.elive.aw2tw.ios
//    8216 com.egls.zh2kr.ios
//    9601 com.elive.sfp.ios           三国先锋
//    9709 com.elive.cls.ios           武神
//    8155 com.eglsgamei.ah2           1.01.07 国服战魂2 
//    9853 com.elive.magictw.ios
   // [[EGLSSDK sharedInstance] setGamePolicy:GamePolicy_tw_in99];
    
    [[EGLSSDK sharedInstance] sdkInitWithAppID:@"8155"
                             withClientVersion:@"1.01.07"
                           withPassportCountry:PassportCountry_sandbox
                                   withIsDebug:YES
                          withCallBackDelegate:self];
    
    //[EGLSSDK setBannerSwitchHidden:NO];
    
    //[EGLSSDK
//    [EGLSSDK setEvent_one_splash_image];
//    [EGLSSDK setEvent_tutorial_start];
//    [EGLSSDK setEvent_Customize:@"123" withValue:nil];
   // [EGLSSDK setGuestEnable:YES];
    [EGLSSDK setCRLoginOpen:YES];
//    [EGLSSDK setBannersEnable:NO];
    [EGLSSDK setContentAlertEnable:NO];
    [EGLSSDK setWXLoginOpen:NO];
    //[self motionTest1];
  //  [EGLSSDK setBannersEnable:NO];
     //[EGLSSDK setBannerSwitchHidden:NO];
    //[EGLSSDK setFloatButton_show:NO];
   //  [EGLSSDK setCRLoginOpen:YES];
    [EGLSSDK setDebugOpen:false];
    
    
    // [[EGLSSDK sharedInstance] setUsePurchaseSandbox:YES];
   
    
   // [SVProgressHUD show];
  //  [EGLSSDK setUIModalPresentationStyle:UIModalPresentationFullScreen];
    
    // [EGLSSDK setSetPaymentCondition:YES];
//    NSString *localeLanguageCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"][0];
//    
//    NSLog(@"AppleLanguages = %@",localeLanguageCode);
//    NSLog(@"AppleLanguages = %@",localeLanguageCode);
    
    
    //""
    //[message.body stringByRemovingPercentEncoding]
    
   //  NSString *  message1 =@"\\U67e5\\U7121\\U767c\\U7968\\U8cc7\\U6599";
   //  NSLog(@"userContentController1 %@",[message1 stringByRemovingPercentEncoding]);
    
//    NSString  * aearText =  @"00886";
//
//    int  number = [aearText intValue];
//    
//    NSLog(@"number = %d",number);
//    NSLog(@"");
   /// NSDictionary * dic = [EGLSSDKUtil dictionaryWithJsonString:@"\U67e5\U7121\U767c\U7968\U8cc7\U6599"];
   // ;
    //NSLocalizedStringFromTableInBundle(@"Guest", nil, MYBUNDLE, nil);
   
    
    return YES;
}


-(UITextField *) getUITextField:(CGRect)rectMake placeholder:(NSString*)placeholder{
    //输入框
       //初始化textfield并设置位置及大小
       UITextField * _textFiled = [[UITextField alloc]initWithFrame:rectMake];
       //设置边框样式，只有设置了才会显示边框样式
       //再次编辑就清空
       _textFiled.clearsOnBeginEditing = YES;

       //内容对齐方式
       _textFiled.textAlignment = UITextAlignmentLeft;

       //内容的垂直对齐方式  UITextField继承自UIControl,此类中有一个属性contentVerticalAlignment

       _textFiled.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;

       //设置为YES时文本会自动缩小以适应文本窗口大小.默认是保持原来大小,而让长文本滚动
       _textFiled.adjustsFontSizeToFitWidth = YES;

       //设置自动缩小显示的最小字体大小
       _textFiled.minimumFontSize = 20;

       //设置键盘的样式
       _textFiled.keyboardType = UIKeyboardTypeDefault;
    
    //return键变成什么键
      _textFiled.returnKeyType =UIReturnKeyDefault;

       //设置输入框的背景颜色，此时设置为白色 如果使用了自定义的背景图片边框会被忽略掉
       _textFiled.backgroundColor = [UIColor whiteColor];
  
    
     //UIImage * imageBack = [UIImage imageNamed:@"EGLSSDKRES.bundle/img_official_login_account.png"];
    
    
       //设置背景
     //  _textFiled.background = imageBack;

       //设置背景
      // _textFiled.disabledBackground = imageBack;

       //当输入框没有内容时，水印提示 提示内容为password
       _textFiled.placeholder = placeholder;

       //设置输入框内容的字体样式和大小
       _textFiled.font = [UIFont fontWithName:@"Arial" size:20.0f];

       //设置字体颜色
       _textFiled.textColor = [UIColor redColor];

       //输入框中是否有个叉号，在什么时候显示，用于一次性删除输入框中的内容
       _textFiled.clearButtonMode = UITextFieldViewModeAlways;
    
        _textFiled.delegate =self;
    return _textFiled;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{

//返回一个BOOL值指明是否允许根据用户请求清除内容

//可以设置在特定条件下才允许清除内容

     return YES;

}
//-(IBAction)textFieldDoneEditing:(id)sender
//{
//  [sender resignFirstResponder];
//}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{

//返回一个BOOL值，指明是否允许在按下回车键时结束编辑

 //如果允许要调用resignFirstResponder 方法，这回导致结束编辑，而键盘会被收起[textField resignFirstResponder];

//查一下resign这个单词的意思就明白这个方法了
    [textField resignFirstResponder];
     return YES;

}

-(void) eglsSdkInitCallBack:(NSString *)state andJsonVaule:(NSString *) json;
{
    NSLog(@"初始化成功");
//    [EGLSSDK collectionServerEvent:@"event_one_splash_image" andVaule:nil];
//    [EGLSSDK collectionServerEvent:@"event_new_character" andVaule:nil];
//    [EGLSSDK collectionServerEvent:@"event_one_update_start" andVaule:nil];
//    [EGLSSDK collectionServerEvent:@"event_one_update_complete" andVaule:nil];
}
-(void)touchAgreementCallBack:(BOOL)state
{
    
    //[EGLSSDK collectionServerEvent:@"event_terms_agree" andVaule:nil];
    NSLog(@"222222");
    // [[EGLSSDKUtil sharedInstance] loginCR];
}
-(void)weixin
{
}


-(void)first
{
    //[[EGLSSDK sharedInstance] firstTime:@"character_name"];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    //之所以里面限制内容这么写，是因为，如果用户回删内容的话，仅仅if(textField.text.length>11){ return NO  }else  return YES;是不能够完美实现要求的，大家可以去试试
    
    if(textField.text.length==20){
        
        if ([string isEqualToString:@""]) {
            
            return YES;
            
        }else
            
            return NO;
        
    }else
        
        return YES;
    
    
}
- (void) alert {
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil
                                                     message:@"透传了"
                                                    delegate:self
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
    [alert show];
    //[alert release];

}


- (void)autoLoginClick {
    //
  //  [[EGLSSDK sharedInstance] autoLogin];
   // [[EGLSSDK sharedInstance] eglsLogOut];
   // [[EGLSSDK sharedInstance] eglsLogin:MODE_LOGIN_AUTO];
//    dispatch_async(dispatch_get_main_queue(), ^(void)
//     {
//           [SVProgressHUD show];
//    });
  
    
    
    //NSString * account =  _accountField.text;
   // [[EGLSSDK sharedInstance] mobileRegisterVerifyLightly:account];
    //NSLog(@"%@",account);
    
    //采用默认登录
    
    
   // [SVProgressHUD show];
   // [[EGLSSDK sharedInstance] accountQuery];
   
//    NSString * account =  _accountField.text;
//    NSString * verifyfield =  _verifyField.text;
//    NSString * password =  _passWordField.text;
//
//    [[EGLSSDK sharedInstance] mobileLoginLightly:account passWord:password];
    
   // [[EGLSSDK sharedInstance] channelLoginLightly:@"3"];
   //
    //eglsLogOut
}

- (void)showLoginClick {
    
    
//      NSString * account =  _accountField.text;
//      NSString * verifyfield =  _verifyField.text;
//      NSString * password =  _passWordField.text;
//
//    [[EGLSSDK sharedInstance] channelLoginLightly:@"7"];
  //  [[EGLSSDK sharedInstance] channelLoginLightly:@"0"];
   // [[EGLSSDK sharedInstance] pwdResetCaptchaLightly:account];
  //  [[EGLSSDK sharedInstance] mobileRegisterRequestLightly:account verificationCode:verifyfield passWord:password];
    //[[EGLSSDK sharedInstance] mobileLoginLightly:account passWord:password];
   // [[EGLSSDK sharedInstance] eglsLogin:MODE_LOGIN_COMMON];
    [[EGLSSDK sharedInstance] eglsLogin:MODE_LOGIN_COMMON];
    //[[EGLSSDK sharedInstance] showLogin];
//    NSLog(@"[AppDelegate getCurrentVC] = %@",[AppDelegate getCurrentVC]);
    
}

- (void)chargeClick {
      NSString * account =  _accountField.text;
      NSString * verifyfield =  _verifyField.text;
      NSString * password =  _passWordField.text;
    
     [[EGLSSDK sharedInstance] pwdResetRequestLightly:account captcha:verifyfield];
    
     // [[EGLSSDK sharedInstance] mobileBindRequestLightly:account verificationCode:verifyfield passWord:password];
    // [[EGLSSDK sharedInstance] applePurchaseCommunal:PAYTYPE_EB productID:@"com.linkedfun.koc.tw.ltdtreasure1" orderID:@"" productName:@"钻石" price:0.99 isSandBox:true extra:@""];
   
 //   [EGLSSDK  NaverControllerShow];
//    [[EGLSSDK sharedInstance] showChargeWithProductID:@"com.eglsgame.paltform.lib.test" withAmount:@"10" withProductName:LS(@"OneBagOfDiamonds", nil) withExtraData:@"handsome"];
//
//    [[EGLSSDK sharedInstance] showChargeWithProductID:@"rw.us.6rmbbx1" withAmount:@"10" withProductName:LS(@"OneBagOfDiamonds", nil) withExtraData:@"handsome" withSubgame:@"100005" with、:@"149680001" withLevel:@"2" withVipLevel:@"0"];
    
            //     SKMutablePayment* payment = [SKPayment paymentWithProductIdentifier:@"zh2.cn.30rmbtest"];
//                   payment.quantity = 0;
//                   payment.applicationUsername = @"";

              //     [[SKPaymentQueue defaultQueue] addPayment:payment];
    
    //[[EGLSSDK sharedInstance] updateRoleData:@"1003001003" roleID:@"3287460094" level:@"0" vipLevel:@"0"];
 
 //   [[EGLSSDK sharedInstance] applePurchaseECWithProductID:@"aw2.tw.30twdsp1" withExtraData:@"" productName:@"钻石" price:30];
    
   // [[EGLSSDK sharedInstance] applePurchaseCommunal:PAYTYPE_COMMON productID:@"zh2.cn.30rmbtest" orderID:@"" productName:@"钻石" price:30 isSandBox:true extra:@""];
    //[[EGLSSDK sharedInstance] applePurchaseCommunal:PAYTYPE_COMMON productID:@"zh2.cn.6rmb" orderID:@"" productName:@"钻石" price:30 isSandBox:true extra:@""];
    //rwby.tw.150twd
    //[[EGLSSDK sharedInstance] applePurchaseWithProductID:@"rw.us.6rmbbx1" withExtraData:@""];
    
}
- (void)requestCallbackPlayerInfo:(int)state andMessage:(NSString*) josn
{
    NSLog(@"requestCallbackPlayerInfo state = %d and message = %@",state,josn);
      NSLog(@"end");
}
- (void)applePurchaseClick {
    
   // [[EGLSSDK sharedInstance] updateRoleData:@"1003001003" roleID:@"3287460094" level:@"0" vipLevel:@"0"];
    
    //   [[EGLSSDK sharedInstance] applePurchaseECWithProductID:@"aw2.tw.30twdsp1" withExtraData:@"" productName:@"钻石" price:30];
  //  com.mmtw.mc.pro53m
     
    
    [[EGLSSDK sharedInstance] channelPurchaseLightly:@"14.99" productId:@"com.mmtw.ios.mon30" productName:@"月卡" orderInfo:@"" flag:FLAG_PURCHASE_DEFAULT];
    //[[EGLSSDK sharedInstance] applePurchaseCommunal:PAYTYPE_EB productID:@"com.lianqu.godofgemble.chippacklogin" orderID:@"" productName:@"钻石" price:0.99 isSandBox:true extra:@""];
    
       
  //     [[EGLSSDK sharedInstance] applePurchaseCommunal:PAYTYPE_COMMON productID:@"zh2.cn.6rmb" orderID:@"" productName:@"钻石" price:30 isSandBox:true extra:@""];
    
    // [[EGLSSDK sharedInstance] ratingActivity];
     // [[EGLSSDK sharedInstance] facebookInviteFriends];
//    [[EGLSSDK sharedInstance] applePurchaseWithProductID:@"kkk.tw.ios.490twd" withExtraData:nil];
//
//    [[EGLSSDK sharedInstance] linePromotionActivity];
//
//    [[EGLSSDKUtil sharedInstance] openWeb:@"https://baidu.com"];
//
    
    
//    [[EGLSSDK sharedInstance] applePurchaseMCWithProductID:@"aw2.tw.mc50twd" withExtraData:@"123" productName:@"测试" price:50 isSandBox:NO];
//    [[EGLSSDK sharedInstance] applePurchaseMCWithProductID:@"tltc.tw.mc.150twd" withExtraData:@"20181128160018555903948327" productName:@"60元寶" price:150.00 isSandBox:YES];
   // [[EGLSSDK sharedInstance] applePurchaseWithProductID:@"rw.us.6rmbbx1" withExtraData:nil withSubgame:@"1003002" withRoleID:@"7200525660" withLevel:@"1" withVipLevel:@"0" productName:@"钻石" price:6.0 discount:0 quantity:0 currency:nil category:@"" extraAttrsMap:@""];
    
    // [[EGLSSDK sharedInstance] applePurchaseWithProductID:@"rw.us.6rmbbx1" withExtraData:@""];
//    [[EGLSSDK sharedInstance] applePurchaseWithProductID:@"com.g2us.zh.6rmb" withExtraData:@"5304114841718220328|mg.tw.30twd"];  // @"order123654789" com.egls.sdk.test   com.egls.moba.tw.6rmb     com.fantasykk.kk.zhtw.diamond60     com.egls.moba.tw.6rmb     kcj.cn.6rmb  com.g2us.zh.6rmb mg.tw.30twd
    
   // [[EGLSSDK sharedInstance] applePurchaseWithProductID:@"mg.tw.30twd" withExtraData:@"5304114841718220328#mg.tw.30twd"];  // @"order123654789" com.egls.sdk.test   com.egls.moba.tw.6rmb     com.fantasykk.kk.zhtw.diamond60     com.egls.moba.tw.6rmb     kcj.cn.6rmb  com.g2us.zh.6rmb mg.tw.30twd
}
- (void)verifyCallbackFromRegister:(int)state andMessage:(NSString*)message {
    
    NSLog(@"verifyCallbackFromRequest state = %d and message = %@",state,message);
    NSLog(@"end");
}

-(void)requestCallbackFromChangePassWord:(int)state andMessage:(NSString*)message{
    NSLog(@"requestCallbackFromChangePassWord state = %d and message = %@",state,message);
    NSLog(@"end");
}


- (void)captchaCallbackFromPassWord:(int)state andMessage:(NSString*)message{
    NSLog(@"captchaCallbackFromPassWord state = %d and message = %@",state,message);
       NSLog(@"end");
}


- (void)requestCallbackFromResetPassWord:(int)state andMessage:(NSString*)message{
    NSLog(@"requestCallbackFromResetPassWord state = %d and message = %@",state,message);
    NSLog(@"end");
}

-(void)channelBindCallBack:(NSString *)accountType withNickName:(NSString *)nickName withBindState: (int) state withMessage:(NSString*)massage{
    NSLog(@"channelBindCallBack!\n  state is:%d channel:%@ nick:%@ message:%@",state, accountType,nickName,massage);
    NSLog(@"");
}


-(void)loginCallback:(int)state andToken:(NSString *)token anduid: (NSString *)uid accountType:(NSString *)channel andNickname:(NSString *)nick andMassage:(NSString *) massage {
    NSLog(@"EGLS Login  Call Back!\n  state is:%d uid is : %@\n token is : %@ channel:%@ nick:%@ message:%@",state, uid, token,channel,nick,massage);
    NSLog(@"");
}

-(void)FBloginSuccessCallBackWithUid:(NSString *)uid name:(NSString *)name picture:(NSString *)picture
{
     NSLog(@"EGLS Login Success Call Back!\n uid is : %@\n name is : %@ picture:%@", uid, name,picture);
}
-(void)FBloginSuccessCallBackWithFields:(NSArray *)fields
{
    NSLog(@"field is:%@",fields);
}
- (void)loginCancel {
    NSLog(@"EGLS loginCancel");
}

- (void)registerSuccessCallBackWithUid:(NSString *)uid {
    NSLog(@"registerSuccess!\n uid is : %@", uid);
}

- (void)applePurchaseCallBackWithOrderID:(int)state andOrderId:(NSString *)orderID andOtherMessage:(NSString *)money{
    NSLog(@"applePurchaseSuccessCallBack!\n orderID is : %@\n money is : %@", orderID, money);
}

//- (void)applePurchaseFailCallBack:(NSString *)failState
//{
//     NSLog(@"applePurchaseFailCallBack!\n failState is %@", failState);
//
//}
#pragma mark fb
- (void)fbShareImgClick {
    UIImage * image = [UIImage imageNamed:@"EGLSSDKRES.bundle/kr_73.png"];
    [[EGLSSDK sharedInstance] facebookShareWithImage:image];
}

- (void)fbShareUrlAndSoOnClick {
    
    NSURL * url = [NSURL URLWithString:@"http://www.g2us.com"];
    NSURL * imgUrl = [NSURL URLWithString:@"https://res.oasgames.com/platform/uploader/1394777516.jpg"];
    NSString * title = LS(@"EGLS", nil);
    NSString * contentDescription = LS(@"GoodProduct", nil);
    [[EGLSSDK sharedInstance] facebookShareWithUrl:url withImageUrl:imgUrl withTitle:title withContentDescription:contentDescription];
}

- (void)fbInviteFriends {
    
    [[EGLSSDK sharedInstance] updateRoleData:@"1003001003" roleID:@"3287460094" level:@"0" vipLevel:@"0"];
    [[EGLSSDK sharedInstance] applePurchaseCommunal:PAYTYPE_COMMON productID:@"zh2.cn.6rmbtest3" orderID:@"" productName:@"钻石" price:30 isSandBox:true extra:@""];
    
    //[[EGLSSDK sharedInstance] facebookInviteFriends];
}
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"str=%@",[[[[deviceToken description] stringByReplacingOccurrencesOfString: @"<" withString: @""]
                  stringByReplacingOccurrencesOfString: @">" withString: @""]
                 stringByReplacingOccurrencesOfString: @" " withString: @""]);
    
    NSLog(@"deviceToken1=%@",deviceToken);


}
- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    //Device Token 登录失败 Log 确认
    NSLog(@"Device Token Register Failed: %@", error);
    
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_7_0
#warning "Remote push open tracking is counted only when user touches notification center under iOS SDK 7.0"

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    //[LiveOpsPush handleRemoteNotification:userInfo fetchHandler:nil];
}
#else
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    [[EGLSSDK sharedInstance] application:application didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];

}
#endif

- (void)application:(UIApplication*)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    

}


- (void)facebookShare_DidCompleteWithResults:(NSDictionary *)results {
    NSLog(@"didCompleteWithResults:results ==%@", results);
}

- (void)facebookShare_DidFailWithError:(NSError *)error{
    NSLog(@"didFailWithError:error == %@", error);
}

- (void)facebookShare_DidCancel{
    NSLog(@"sharerDidCancel");
}

- (void)lineShareImgClick {
    
    
    
    UIImage * image = [UIImage imageNamed:@"EGLSSDKRES.bundle/kr_75.png"];
    if ([[EGLSSDK sharedInstance] lineShareImage:image]) {
        NSLog(LS(@"LineIsOpened", nil));
    } else {
        NSLog(LS(@"LineCanNotBeOpened", nil));
    }
}
-(void)show
{
   

}
- (void)lineShareTextClick {

    NSString * text = @"EGLS is great!";
    if ([[EGLSSDK sharedInstance] lineShareText:text]) {
        NSLog(LS(@"LineIsOpened", nil));
    } else {
        NSLog(LS(@"LineCanNotBeOpened", nil));
    }
}

- (void)userCenterClick {
    [[EGLSSDK sharedInstance] showUserCenter];
}

- (void)applicationDidBecomeActive:(UIApplication *)application{
    [[EGLSSDK sharedInstance] applicationDidBecomeActive:application];
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([[EGLSSDK sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation]) {
        return YES;
    }
    return NO;
}
- (BOOL)application:(UIApplication*)app openURL:(NSURL*)url options:(NSDictionary*)options{
   
    return  [[EGLSSDK sharedInstance] application:app openURL:url options:options];
}
//- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options {
//
//    return  [[EGLSSDK sharedInstance] application:application openURL:url];
//}
- (void)applicationWillTerminate:(UIApplication*)application
{
    [[EGLSSDK sharedInstance] applicationWillTerminate:application];
}
- (void)FBloginInviteCallBackWithFriends:(NSArray *)friends
{
    NSLog(@"%@",friends);
}

+ (UIViewController *)getCurrentVC {
    
    UIWindow *window = [[UIApplication sharedApplication].windows firstObject];
    if (!window) {
        return nil;
    }
    UIView *tempView;
    for (UIView *subview in window.subviews) {
        if ([[subview.classForCoder description] isEqualToString:@"UILayoutContainerView"]) {
            tempView = subview;
            break;
        }
    }
    if (!tempView) {
        tempView = [window.subviews lastObject];
    }
    
    UIResponder *res = [tempView nextResponder];
    
    while (res) {
        NSLog(@"*************************************\n%@",res);
        res = [res nextResponder];
    }
    
    
    id nextResponder = [tempView nextResponder];
    while (![nextResponder isKindOfClass:[UIViewController class]] || [nextResponder isKindOfClass:[UINavigationController class]] || [nextResponder isKindOfClass:[UITabBarController class]]) {
        tempView =  [tempView.subviews firstObject];
        
        if (!tempView) {
            return nil;
        }
        nextResponder = [tempView nextResponder];
    }
    return  (UIViewController *)nextResponder;
}


- (void )motionTest
{
    
    selfMotionManager = [[CMMotionManager alloc] init];  // ①
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];

    if (selfMotionManager.accelerometerAvailable) {
        //设置CMMotionManager的加速度数据更新频率为0.1秒
        selfMotionManager.accelerometerUpdateInterval = 0.1;
        //使用代码块开始获取加速度数据
        [selfMotionManager startAccelerometerUpdatesToQueue:queue withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
            NSString* labelText;
            //如果发生了错误，error不为空
            if (error) {
                //停止获取加速度数据
                [selfMotionManager stopAccelerometerUpdates];
                labelText = [NSString stringWithFormat:@"获取加速度数据出现错误：%@",error];
            }else{
                //分别获取系统在X、Y、Z轴上的加速度数据
                labelText = [NSString stringWithFormat:@"加速度为\n------\nX轴：%+.2f\nY轴：%+.2f\nZ 轴：%+.2f",accelerometerData.acceleration.x,accelerometerData.acceleration.y,accelerometerData.acceleration.z];
            }
            
          
            
        }];
    }else{
       
        NSLog(@"该设备不支持获取加速度数据!");
    }
    //如果CMMotionManager的支持获取陀螺仪数据
    if (selfMotionManager.gyroAvailable) {
        //设置CMMOtionManager的陀螺仪数据更新频率为0.1；
        selfMotionManager.gyroUpdateInterval = 0.1;
        //使用代码块开始获取陀螺仪数据
        [selfMotionManager startGyroUpdatesToQueue:queue withHandler:^(CMGyroData *gyroData, NSError *error) {
            NSString *labelText;
            // 如果发生了错误，error不为空
            if (error){
                // 停止获取陀螺仪数据
                [selfMotionManager stopGyroUpdates];
                labelText = [NSString stringWithFormat:@"获取陀螺仪数据出现错误: %@", error];
            }else{
                // 分别获取设备绕X轴、Y轴、Z轴上的转速
                labelText = [NSString stringWithFormat: @"绕各轴的转速为\n--------\nX轴: %+.2f\nY轴: %+.2f\nZ轴:                %+.2f",gyroData.rotationRate.x,gyroData.rotationRate.y,gyroData.rotationRate.z];
            
            }
       
        }];
    }else{
         NSLog(@"该设备不支持获取陀螺仪数据！");
    }
    //如果CMMotionManager的支持获取磁场数据
    if (selfMotionManager.magnetometerAvailable) {
        //设置CMMotionManager的磁场数据更新频率为0.1秒
        selfMotionManager.magnetometerUpdateInterval = 0.1;
        [selfMotionManager startMagnetometerUpdatesToQueue:queue withHandler:^(CMMagnetometerData *magnetometerData, NSError *error) {
            NSString* labelText;
            //如果发生了错误 error不为空
            if (error) {
                //停止获取磁场数据
                [selfMotionManager stopMagnetometerUpdates];
                labelText = [NSString stringWithFormat:@"获取磁场数据出现错误:%@",error];
            }else{
                labelText = [NSString stringWithFormat:@"磁场数据为\n--------\nX轴: %+.2f\nY轴: %+.2f\nZ轴:      %+.2f",magnetometerData.magneticField.x,magnetometerData.magneticField.y,magnetometerData.magneticField.z];
            }
            //在主线程中更新magnetometerLabel的文本，显示磁场数据
      
        }];
    }else{
        
          NSLog(@"该设备不支持获取磁场数据！");
    }
}
- (void )motionTest1
{
    
    
    //初始化全局管理对象
    CMMotionManager * manager = [[CMMotionManager alloc] init];
    selfMotionManager = manager;
    
    if ([selfMotionManager isDeviceMotionAvailable]) {
        manager.deviceMotionUpdateInterval = 1/50;
        [selfMotionManager startDeviceMotionUpdatesToQueue:[NSOperationQueue mainQueue]
                                                withHandler:^(CMDeviceMotion * _Nullable motion,
                                                              NSError * _Nullable error) {
                                                    
                                                    // Gravity 获取手机的重力值在各个方向上的分量，根据这个就可以获得手机的空间位置，倾斜角度等
                                                    double gravityX = motion.gravity.x;
                                                    double gravityY = motion.gravity.y;
                                                    double gravityZ = motion.gravity.z;
                                                    
//                                                    double attitudeX =    motion.attitude.pitch/ M_PI * 180.0;
//                                                    double attitudeY =    motion.attitude.roll/ M_PI * 180.0;
//                                                    double attitudeZ =    motion.attitude.yaw/ M_PI * 180.0;
                                                    
//                                                    double  rateX =   motion.rotationRate.x;
//                                                    double  rateY =   motion.rotationRate.y;
//                                                    double  rateZ =   motion.rotationRate.z;
                                                    
                                                     //NSLog(@"旋转角度 x=  %.4f,y=  %.4f,z=  %.4f", attitudeX, attitudeY,attitudeZ);
                                                    // 获取手机的倾斜角度(zTheta是手机与水平面的夹角， xyTheta是手机绕自身旋转的角度)：
                                                    double zTheta = atan2(gravityZ,sqrtf(gravityX * gravityX + gravityY * gravityY)) / M_PI * 180.0;
                                                    double xyTheta = atan2(gravityX, gravityY) / M_PI * 180.0;

                                                    NSLog(@"手机与水平面的夹角 --- %.4f, 手机绕自身旋转的角度为 --- %.4f", zTheta, xyTheta);
                                                }];
    }
   
}

-(void)closeMotion
{
    [selfMotionManager stopDeviceMotionUpdates]; // 停止所有的设备
    //    [self.motionManager stopAccelerometerUpdates]; // 加速度计
    //    [self.motionManager stopMagnetometerUpdates]; // 磁力计
    //    [self.motionManager stopGyroUpdates]; // 陀螺
    

    
}
@end
